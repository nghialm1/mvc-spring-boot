package com.example.MVC.model;

import com.example.MVC.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductServiceImpl implements IProductService {
  @Autowired
  private ProductRepository repo;

  //get all product
  @Override
  public List<Product> listAll() {
    return repo.findAll();

  }
  //Save
  @Override
  public void save(Product product) {
    repo.save(product);
  }
  //get product by ID
  @Override
  public Product get(int id) {
    return repo.getOne(id);
  }

  //delete product by ID
  @Override
  public void delete(int id) {
    repo.deleteById(id);
  }
}
