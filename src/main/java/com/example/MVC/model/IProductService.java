package com.example.MVC.model;

import com.example.MVC.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IProductService {
  public List<Product> listAll();

  public void save(Product product);

  public Product get(int id);

  public void delete(int id);
}
