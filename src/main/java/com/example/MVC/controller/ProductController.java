package com.example.MVC.controller;

import com.example.MVC.entity.Product;
import com.example.MVC.model.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ProductController {

  @Autowired
  private ProductServiceImpl service;

  //return homepage - list product
  @RequestMapping("/")
  public String viewHomePage(Model model) {
    List<Product> listProducts = service.listAll();
    model.addAttribute("listProducts", listProducts);
    return "index";
  }

  //return view create product
  @RequestMapping("/new")
  public String showNewProductForm(Model model) {
    //tao kieu object cho view
    Product product = new Product();
    model.addAttribute("product", product);
    return "new_product";
  }

  //save product
  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public String saveProduct(@ModelAttribute("product") Product product) {
    service.save(product);
    //return home page
    return "redirect:/";
  }

  //
  @RequestMapping("/edit/{id}")
  public String showEditProductForm(@PathVariable(name = "id") int id, Model model) {
    Product product = service.get(id);
    model.addAttribute("product", product);
    return "edit_product";
  }

  @RequestMapping("/delete/{id}")
  public String deleteProduct(@PathVariable(name = "id") int id) {
    service.delete(id);
    return "redirect:/";
  }
}
