package com.example.MVC;


/**
 * Author: Minh Nghia
 * Date: 18-01-2021
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MvcApplication{


  public static void main(String[] args) {
    SpringApplication.run(MvcApplication.class, args);
  }


}
